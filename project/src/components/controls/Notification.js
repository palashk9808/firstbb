import { makeStyles, Snackbar } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import React from 'react'

const useStyles = makeStyles(theme => ({
    root:{
        fontSize: "20px"
    }
}))

export default function Notification(props){

    const {notify, setNotify} = props
    const classes=useStyles()

    const handleClose = (event, reason)=>{
        setNotify({
            ...notify,
            isOpen:false
        })
    }

    return(
        <div>
            <Snackbar 
            className={classes.root}
            open={notify.isOpen}
            autoHideDuration={3000}
            onClose={handleClose}
            >
                <Alert 
                className={classes.root}
                severity = {notify.type}
                onClose={handleClose}>
                    {notify.message}
                </Alert>
            </Snackbar>
        </div>
    )
}