import { InputLabel, TextField } from '@material-ui/core';
import React, { Component } from 'react';

export default function Input(props){
    const {name,label,value,onChange,error=null,disabled,type}=props;
    return(
        <div className="row">
        {/* <InputLabel className="col">{label}</InputLabel> */}
        <TextField className="col" variant="outlined" 
        placeholder ={` ${label}`} 
        name={name} 
        value={value} 
        type={type}
        onChange={onChange}
        disabled={disabled} 
        // error
        // helperText={`Please enter a valid ${label}`}
        {...(error && {error:true,helperText:error})} />
        </div>
    )
}