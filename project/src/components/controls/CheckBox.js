import { Checkbox, FormControl, FormControlLabel } from '@material-ui/core';
import React, { Component } from 'react';

export default function CheckBox(props){
    const {name,label,value,onChange} = props;

    // const convertToDefEventPara=(name,value)=>({
    //     target:{
    //         name, value

    //     }
    // })
    return(
        <div>
            <FormControl>            
                <FormControlLabel control={
                    <Checkbox 
                    name={name}
                    color="primary"
                    checked={value} 
                    onChange={onChange}/>} 
                    label={label}/>
            </FormControl>
        </div>
        // e=>onChange(convertToDefEventPara(name,e.target.checked))
    )
}