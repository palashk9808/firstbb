import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { Button, Typography } from '@material-ui/core';


export default function DialogBox(props){

    const {title, children, openPopup, setOpenPopup, notify, setNotify} = props;

    
    const message = (title == "Create Transaction")?("Are you sure you want to create this transaction?"):('Are you sure you want to save this transaction as a draft?');

    const handleClose = () => {
        setOpenPopup(false);
        setNotify({
            isOpen:true,
            message: "Submitted Successfully!",
            type: "success"
        })
      };

      const Transition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
      });
    
    return(
        <Dialog 
        open={openPopup} 
        // TransitionComponent={Transition}
        // keepMounted
        onClose={handleClose}
        maxwidth="md">
            <DialogTitle>
                <div>
                    <Typography variant="h6" component="div" >
                    {title?title:"Confirm?"}
                    </Typography>
                </div>
            </DialogTitle>

            <DialogContent dividers>
                <div> {message}</div>
            </DialogContent>

            <DialogActions>
            <Button href =""
                    variant="contained"
                    //  onClick={()=>handleSubmit("Create Transaction")}
                    style={{backgroundColor:"#2884f0",color:"white"}}
                    onClick={handleClose}
                    >Cancel
                    </Button>

                    <Button href =""
                    variant="contained"
                    onClick={handleClose}
                    style={{backgroundColor:"#40d50a",color:"white"}}>Confirm
                    </Button>
            </DialogActions>
        </Dialog>
    )

}

// const Transition = React.forwardRef(function Transition(props, ref) {
//     return <Slide direction="up" ref={ref} {...props} />});

// export default function DialogBox(props){




//     const [open, setOpen] = React.useState(false);
    
//     const handleClickOpen = () => {
//         setOpen(true);
//     };
    
//     const handleClose = () => {
//         setOpen(false);
//     };
//     return(
//         <div>
//             <Dialog
//                 open={open}
//                 TransitionComponent={Transition}
//                 keepMounted
//                 onClose={handleClose}
//             >
//                 <DialogTitle id="alert-dialog-slide-title">{"Use Google's location service?"}</DialogTitle>
//                 <DialogContent>
//                 <DialogContentText id="alert-dialog-slide-description">
//                     Let Google help apps determine location. This means sending anonymous location data to
//                     Google, even when no apps are running.
//                 </DialogContentText>
//                 </DialogContent>
//                 <DialogActions>
//                 <Button onClick={handleClose} color="primary">
//                     Disagree
//                 </Button>
//                 <Button onClick={handleClose} color="primary">
//                     Agree
//                 </Button>
//                 </DialogActions>
//             </Dialog>
//         </div>
//     )
// }