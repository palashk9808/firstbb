import { MenuItem, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import React from 'react';

export default function AutoComplete(props){
    const{name,label,value,onChange, error=null,options} = props;
    // console.log("inside autocomplete", value)
    return(
        <div>
            <Autocomplete
             name={name} 
             options={
                 options
             } 
             value={value}
             getOptionLabel={(option) =>  typeof option === 'string' ? option : option.title}
             renderInput={(params) => <TextField {...(error && {error:true,helperText:error})} {...params} label={label} variant="outlined" />}
             onChange={onChange}
             autoComplete= {true}
             getOptionSelected = {(option, value) => option.title===value}             
             />

        </div>
    )
}

// .map((item,index)=> (
//     <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>
// ))