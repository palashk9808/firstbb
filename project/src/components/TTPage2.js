import React, {useState,useEffect} from "react";
import { Button, ButtonGroup,createTheme,FormGroup, InputLabel,makeStyles,TextField } from "@material-ui/core";
import { ThemeProvider,createGenerateClassName } from "@material-ui/styles";
import { Typography, Container, Paper, Grid } from "@material-ui/core";
import AutoComplete from "./controls/AutoComplete";
import Input from "./controls/Input";
import { BrowserRouter, Link } from "react-router-dom";
import CheckBox from "./controls/CheckBox";
import DialogBox from "./controls/DialogBox"
import Notification from "./controls/Notification";


const useStyles= makeStyles(theme=>({
    pageContent:{
      margin: theme.spacing(5),
      padding: theme.spacing(2),
      fontSize: "20px"
    }
  }))
const useStyles2= makeStyles(theme=>({
    root:{
        '& .MuiFormControl-root':{
            width: "80%",
            margin: theme.spacing(1),
            fontSize: "30px"
        }
    }
}))

 const theme = createTheme({
    palette:{
        primary:{
            main:"#2884f0"
        },
        secondary:{
            main: "#40d50a"
        }

    }
}) 
  
export default function TTPage2(){

    const classes=useStyles();
    const classes2=useStyles2();

//initial values
    const initialValues={
        refNo: "101DEFAULT",
        debitAC:"",
        debitCurr:"",
        debitBank:"",
        creditAC:"",
        creditName:"",
        creditSwift:"",
        creditBank:"",
        oldBene:"",
        saveBeneficiary:true,
        comments:"",
        dateOfTransfer: "",
        currency:"Euro (EUR)",
        amount:"",
        fxAmount:""
    }

//setStates
    const [values,setValues]= useState(initialValues);
    const [errors,setErrors] = useState({})
    const [openPopup,setOpenPopup] = useState(false);
    const [buttonType,setButtonType]= useState("");
    const [notify,setNotify] = useState({isOpen:false, message:"", type:"success"})

    //debit dropdown info
    const acs=[
        {id:"1", title:"00000000001"},
        {id:"2", title:"00000000102"},
        {id:"3", title:"00000010103"},
        {id:"4", title:"00001010104"},
        {id:"5", title:"00011100005"}
    ]
    
    const banks=[
        {id:"1", title:"SCB Bangalore"},
        {id:"2", title:"SCB Singapore"},
        {id:"3", title:"SCB Paris"},
        {id:"4", title:"SCB New York"},
        {id:"5", title:"SCB Tokyo"}
    ]
    
    const currencies=[
        {id:"1", title:"Euro(EUR)"},
        {id:"2", title:"American Dollar(USD)"},
        {id:"3", title:"Singaporean Dollar(SGD)"},
        {id:"4", title:"Indian Rupee(INR)"},
        {id:"5", title:"Japanese Yen (JPY)"}
    ]

    //debit functions
    const changeBank=(e,v)=>{
        setValues({
            ...values,
            ["debitBank"]:(v)?v.title:""
        })
        console.log(values)
    }
    
    const changeAC=(e,v)=>{
        setValues({
            ...values,
            ["debitAC"]:(v)?v.title:""
        })
        console.log(values)
    }
    
    const changeCurr=(e,v)=>{
        setValues({
            ...values,
            ["debitCurr"]:(v)?v.title:""
        })
        console.log(values)
    }

    //credit dropdown info
    const swifts=[
        {id:"1", title:"IN101010BAN"},
        {id:"2", title:"SG101010SNG"},
        {id:"3", title:"FR101010PAR"},
        {id:"4", title:"US101010NYC"},
        {id:"5", title:"JP101010TKY"}
    ]

    const banks2=[
        {id:"1", title:"SCB Bangalore"},
        {id:"2", title:"SCB Singapore"},
        {id:"3", title:"SCB Paris"},
        {id:"4", title:"SCB Boston"},
        {id:"5", title:"SCB Abu Dhabi"}
    ]

    const oldBenes=[
        {id:"1", title:"SCB Palash"},
        {id:"2", title:"SCB Nirmit"},
        {id:"3", title:"SCB Nilesh"},
        {id:"4", title:"SCB Nishant"},
        {id:"5", title:"SCB Owais"}
    ]

    //credit functions
    const changeInput=(e)=>{
        const {name,value}=e.target;
        setValues({
            ...values,
            [name]:(value)
        })
        console.log(values)
    }
    const changeChecked = e =>{
        const val = e.target.checked;
        console.log(e.target)
        setValues({
            ...values,
            ["saveBeneficiary"]:val
        })
        console.log(values)
    }
    const changeSwiftAuto=(e,v)=>{
    
        setValues({
            ...values,
            ["creditSwift"]:(v)?v.title:""
        })
        console.log(values)
    }
    const changeBankAuto=(e,v)=>{
        setValues({
            ...values,
            ["creditBank"]:(v)?v.title:""
        })
        console.log(values)
    }
    const changeOldBeneAuto=(e,v)=>{
        setValues({
            ...values,
            ["oldBene"]:(v)?v.title:""
        })
        console.log(values)
    }

    //transaction dropdown info
    const isDraft = false;
    const conversionRate = 80;

    //transaction functions
    const changeAmount=e=>{
        const {value}=e.target;
        setValues({
            ...values,
            ["amount"]:value,
            ["fxAmount"]: conversionRate*value
        })
        console.log(values)
    
    }
    const changeDate=(e)=>{
        const {value}=e.target;
        setValues({
            ...values,
            ["dateOfTransfer"]:(value)
        })
        console.log(values)
    }

    //errors
    const validate = ()=>{
        let temp={}
        temp.debitBank = values.debitBank?"":"This field is required"
        temp.debitCurr = values.debitCurr?"":"This field is required"
        temp.debitAC = values.debitAC?"":"This field is required"

        temp.creditName = (/^[a-zA-Z ]+$/).test(values.creditName)?"":"This field is required"
        temp.creditBank = values.creditBank?"":"This field is required"
        temp.creditSwift = values.creditSwift?"":"This field is required"
        temp.creditAC = (/^[0-9]{11}/).test(values.creditAC)?"":"This field is required"
        temp.oldBene = values.oldBene?"":"This field is required"

        temp.comments = (/^[a-zA-Z ]+$/).test(values.comments)?"":"This field is required"
        temp.dateOfTransfer = values.dateOfTransfer?"":"This field is required"
        temp.currency = values.currency?"":"This field is required"
        temp.amount =(/^[0-9]+$/).test(values.amount)?"":"This field is required"
        temp.fxAmount =(/^[0-9]+$/).test(values.fxAmount)?"":"This field is required"
        console.log(temp)
        setErrors({
            ...temp
        })
        return Object.values(temp).every(x=> x =="");
    }

    const handleSubmit = e  =>{
   
        e.preventDefault()
        console.log(validate())
        if(validate())
            setOpenPopup(true)
       
    }

    return(
        <div>
            <Grid container className={classes2.pageContent} style={{width:"80%", paddingTop:"2%", margin:"auto"}} justifyContent="flex-start">
                <Grid item>
                    <InputLabel>Reference Number</InputLabel>
                    <Input 
                        label="Reference Number:" 
                        name="Reference Number" 
                        value={values.refNo} 
                        disabled={true}
/>
                </Grid>
            </Grid>

        <div className="DebitPart">
            <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7",marginLeft:"auto", marginRight:"auto"}}>
                <h3 style={{color:"#2884f0"}}> Debit Account Details</h3>
                <ThemeProvider theme={theme}>
                    <form className={classes2.root} autoComplete="off">
                        <Grid container>
                            <Grid item xs={12} sm={6} lg={4}>
                            <AutoComplete 
                                label="Account Number" 
                                name="debitAC" 
                                value={values.debitAC} 
                                onChange={changeAC}
                                options={acs}
                                error={errors.debitAC} />
                            </Grid>

                            <Grid item xs={12} sm={6} lg={4}>
                                <AutoComplete 
                                label="Currency" 
                                name="debitCurr" 
                                value={values.debitCurr} 
                                onChange={changeCurr}
                                options={currencies}
                                error={errors.debitCurr} />
                            </Grid>

                            <Grid item xs={12} sm={6} lg={4}>
                            <AutoComplete 
                                label="Bank Name" 
                                name="debitBank" 
                                value={values.debitBank} 
                                onChange={changeBank}
                                options={banks}
                                error={errors.debitBank} />
                            </Grid>
                        </Grid>
                    </form>
                </ThemeProvider>
            </Paper>
        </div>
    
        <div className="CreditPart">
            <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7",marginLeft:"auto", marginRight:"auto"}}>
                <h3 style={{color:"#2884f0"}}> Credit Account Details</h3>
                <br />
                <ThemeProvider theme={theme}>
                    <form className={classes2.root} autoComplete="off">
                        <Grid container>
                            <Grid item xs={12} sm={6}>

                                <Input 
                                label="Payee Name"
                                name="creditName" 
                                value={values.creditName} 
                                onChange={changeInput}
                                error={errors.creditName} />

                                <AutoComplete 
                                label="SWIFT ID" 
                                name="creditSwift" 
                                value={values.creditSwift}
                                onChange={changeSwiftAuto}
                                options={swifts}
                                error={errors.creditSwift}
                                 />
                            </Grid>

                            <Grid item xs={12} sm={6}>

                                <Input 
                                label="Account Number" 
                                name="creditAC" 
                                value={values.creditAC} 
                                onChange={changeInput} 
                                error={errors.creditAC} />

                                <AutoComplete 
                                label="Bank Name" 
                                name="creditBank" 
                                value={values.creditBank} 
                                onChange={changeBankAuto}
                                options={banks2}
                                error={errors.creditBank} />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CheckBox 
                                onChange={changeChecked}
                                value={values.saveBeneficiary} 
                                label="Save Beneficiary Details"
                                name="saveBeneficiary"/>
                            </Grid>
                            
                            <Grid item xs={12} sm={12}>
                                <h4>OR</h4>
                                <AutoComplete 
                                    label="Select Added Beneficiary" 
                                    name="oldBene" 
                                    value={values.oldBene} 
                                    onChange={changeOldBeneAuto}
                                    options={oldBenes}
                                    error={errors.oldBene} />
                            </Grid>
                            
                        </Grid>
                    </form>
                </ThemeProvider>
            </Paper>
            </div>
    
        <div className="TransactionPart">
            <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7", marginLeft:"auto", marginRight:"auto"}
        }>
                <h3 style={{color:"#2884f0"}}> Transaction Details</h3>
                <ThemeProvider theme={theme}>
                    <form className={classes2.root} autoComplete="off">
                        <Grid container>
                            <Grid item xs={12} sm={6}>

                                <Input 
                                type="date"
                                label="Transaction Date"
                                name="DateOfTransfer" 
                                value={values.DateOfTransfer} 
                                onChange={changeDate}
                                error={errors.DateOfTransfer}  />
  
        
                                <Input 
                                label="currency" 
                                name="currency" 
                                value={values.currency} 
                                onChange={changeInput} 
                                disabled={true}
                                error={errors.currency} />
                            </Grid>

                            <Grid item xs={12} sm={6}>


                                <Input 
                                    label="Amount"
                                    name="amount" 
                                    value={values.amount} 
                                    onChange={changeInput} 
                                    error={errors.amount} />

                                <Input 
                                    label="Converted Amount"
                                    name="fxAmount" 
                                    value={values.fxAmount} 
                                    onChange={changeInput} 
                                    error={errors.fxAmount} />
                            </Grid>

                            <Grid item xs={12} sm={12} lg={8}>
                                <Input 
                                    label="Comments"
                                    name="comments" 
                                    value={values.comments} 
                                    onChange={changeInput}
                                    error={errors.comments}  />
                            </Grid>
                        </Grid>
                    </form>
                </ThemeProvider>
            </Paper>
        </div>

        <Grid container className={classes2.pageContent} style={{width:"80%", margin:"auto"}} justifyContent="center">
            <Grid item>
                <ButtonGroup>

                <Button href =""
                 variant="contained"
                //  onClick={()=>handleSubmit("Create Transaction")}
                 style={{backgroundColor:"crimson",color:"white", paddingLeft:"50px", paddingRight:"50px"}}
                 >Back
                 </Button>

                <Button href =""
                 variant="contained"
                 onClick={handleSubmit}
                 style={{backgroundColor:"#2884f0",color:"white"}}>Save as Draft
                 </Button>

                <Button href =""
                 variant="contained"
                //  onClick={()=>handleSubmit("Create Transaction")}
                onClick= {handleSubmit}
                 style={{backgroundColor:"#40d50a",color:"white"}}
                 >Create Transaction
                 </Button>
                </ButtonGroup>
            </Grid>
        </Grid>
        <DialogBox
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        notify={notify}
        setNotify={setNotify}>

        </DialogBox>
        <Notification
            notify={notify}
            setNotify={setNotify} />
    
        </div>
    )
}


