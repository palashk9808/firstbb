import React, {useState,useEffect} from "react";
import { Button, ButtonGroup,createTheme,FormGroup, InputLabel,makeStyles,TextField } from "@material-ui/core";
import { ThemeProvider,createGenerateClassName } from "@material-ui/styles";
import { Typography, Container, Paper, Grid } from "@material-ui/core";
import AutoComplete from "./controls/AutoComplete";
import Input from "./controls/Input";
import { BrowserRouter, Link } from "react-router-dom";

const useStyles= makeStyles(theme=>({
    root:{
        '& .MuiFormControl-root':{
            width: "80%",
            margin: theme.spacing(1)
        }
    }
}))

const theme = createTheme({
    palette:{
        primary:{
            main:"#2884f0"
        },
        secondary:{
            main: "#40d50a"
        }

    }
})

export default function Transaction(props){
    const initialValues={
        comments:"",
        dateOfTransfer: new Date(),
        currency:"Euro (EUR)",
        amount:0,
        fxAmount:""
    }
    const isDraft = false;
    const conversionRate = 80;

const currencies=[
    {id:"1", title:"Euro(EUR)"},
    {id:"2", title:"American Dollar(USD)"},
    {id:"3", title:"Singaporean Dollar(SGD)"},
    {id:"4", title:"Indian Rupee(INR)"},
    {id:"5", title:"Japanese Yen (JPY)"}
]



const [values,setValues]= useState(initialValues);
// console.log(values);
const classes= useStyles();

const changeInput=e=>{
    console.log("inInput",e.target.value)

    const {name,value}=e.target;
    setValues({
        ...values,
        [name]:value
    })
    console.log(values)
}

const changeAmount=e=>{
    const {value}=e.target;
    setValues({
        ...values,
        ["amount"]:value,
        ["fxAmount"]: conversionRate*value
    })
    console.log(values)

}

return(
    <div>
        <BrowserRouter>
        <ThemeProvider theme={theme}>
            <form className={classes.root} autoComplete="off">
                <Grid container>
                    <Grid item xs={12} sm={6}>

                        <Input 
                        type="date"
                        label="Transaction Date"
                         name="DateOfTransfer" 
                        value={values.DateOfTransfer} 
                        onChange={changeInput} />

   
                        <Input 
                        label="currency" 
                        name="currency" 
                        value={values.currency} 
                        onChange={changeInput} disabled="true"/>
                    </Grid>

                    <Grid item xs={12} sm={6}>


                    <Input 
                        label="Amount"
                         name="amount" 
                        value={values.amount} 
                        onChange={changeInput} />

                    <Input 
                        label="Converted Amount"
                         name="fxAmount" 
                        value={values.fxAmount} 
                        onChange={changeInput} />
                    </Grid>

                    <Grid item xs={12} sm={12} lg={8}>
                    <Input 
                        label="Comments"
                         name="comments" 
                        value={values.comments} 
                        onChange={changeInput} />
                    </Grid>

                    <hr />
                </Grid>
            </form>
        </ThemeProvider>
        </BrowserRouter>
    </div>
)
}