import React, {useState,useEffect} from "react";
import { Button, ButtonGroup,createTheme,FormGroup, InputLabel,makeStyles,TextField } from "@material-ui/core";
import { ThemeProvider,createGenerateClassName } from "@material-ui/styles";
import { Typography, Container, Paper, Grid } from "@material-ui/core";
import AutoComplete from "./controls/AutoComplete";
import Input from "./controls/Input";
import { BrowserRouter, Link } from "react-router-dom";

const useStyles= makeStyles(theme=>({
    root:{
        '& .MuiFormControl-root':{
            width: "80%",
            margin: theme.spacing(1)
        }
    }
}))

const theme = createTheme({
    palette:{
        primary:{
            main:"#2884f0"
        },
        secondary:{
            main: "#40d50a"
        }

    }
})

export default function DebitCard(props){
    const initialValues={
        debitAC:"",
        debitCurr:"",
        debitBank:"",

    }

const acs=[
    {id:"1", title:"00000000001"},
    {id:"2", title:"00000000102"},
    {id:"3", title:"00000010103"},
    {id:"4", title:"00001010104"},
    {id:"5", title:"00011100005"}
]

const banks=[
    {id:"1", title:"SCB Bangalore"},
    {id:"2", title:"SCB Singapore"},
    {id:"3", title:"SCB Paris"},
    {id:"4", title:"SCB New York"},
    {id:"5", title:"SCB Tokyo"}
]

const currencies=[
    {id:"1", title:"Euro(EUR)"},
    {id:"2", title:"American Dollar(USD)"},
    {id:"3", title:"Singaporean Dollar(SGD)"},
    {id:"4", title:"Indian Rupee(INR)"},
    {id:"5", title:"Japanese Yen (JPY)"}
]


const [values,setValues]= useState(initialValues);
// console.log(values);
const classes= useStyles();

const changeBank=(e,v)=>{
    setValues({
        ...values,
        ["debitBank"]:(v)?v.title:""
    })
    console.log(values)
}

const changeAC=(e,v)=>{
    setValues({
        ...values,
        ["debitAC"]:(v)?v.title:""
    })
    console.log(values)
}

const changeCurr=(e,v)=>{
    setValues({
        ...values,
        ["debitCurr"]:(v)?v.title:""
    })
    console.log(values)
}


return(
    <div>
        <BrowserRouter>
        <ThemeProvider theme={theme}>
            <form className={classes.root} autoComplete="off">
                <Grid container>
                    <Grid item xs={12} sm={6} lg={4}>
                    <AutoComplete 
                        label="Account Number" 
                        name="debitAC" 
                        value={values.debitAC} 
                        onChange={changeAC}
                        options={acs} />
                    </Grid>

                    <Grid item xs={12} sm={6} lg={4}>
                        <AutoComplete 
                        label="Currency" 
                        name="debitCurr" 
                        value={values.debitCurr} 
                        onChange={changeCurr}
                        options={currencies} />
                    </Grid>

                    <Grid item xs={12} sm={6} lg={4}>
                    <AutoComplete 
                        label="Bank Name" 
                        name="debitBank" 
                        value={values.debitBank} 
                        onChange={changeBank}
                        options={banks} />
                    </Grid>
                </Grid>
            </form>
        </ThemeProvider>
        </BrowserRouter>
    </div>
)
}