import { Button, ButtonGroup, Container, Grid, makeStyles, Paper } from '@material-ui/core';
import Transaction from './Transaction';
import DebitCard from './DebitCard';
import CreditCard from './CreditCard';



const useStyles= makeStyles(theme=>({
  pageContent:{
    margin: theme.spacing(5),
    padding: theme.spacing(3)
  }
}))

function TransactionPage() {
  const classes=useStyles();
  
const handleSubmit = (errorObj)=>{
    for(let error in errorObj){
        console.log(error)
    }
}
  return (
      <div>
    <div className="DebitPart">
        <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7"}}>
            <h3 style={{color:"#2884f0"}}> Debit Account Details</h3>
        <DebitCard />
        </Paper>
    </div>

    <div className="CreditPart">
        <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7"}}>
        <h3 style={{color:"#2884f0"}}> Credit Account Details</h3>
        <br />
            <CreditCard />
        </Paper>
        </div>

    <div className="TransactionPart">
        <Paper elevation= {3} className={classes.pageContent} style={{width:"80%",backgroundColor:"F2F7F7"}}>
        <h3 style={{color:"#2884f0"}}> Transaction Details</h3>
        <br />
        <Transaction />
        </Paper>
    </div>
p
    <Grid container className={classes.pageContent} style={{width:"80%"}} justifyContent="center">
        <Grid item>
            <ButtonGroup>
            <Button href ="" variant="contained"style={{backgroundColor:"#2884f0",color:"white"}}>Save as Draft</Button>
            <Button href =""
             variant="contained"
             onClick={handleSubmit}
             title 
             style={{backgroundColor:"#40d50a",color:"white"}}
             >Create Transaction</Button>
            </ButtonGroup>
        </Grid>
    </Grid>

    </div>
  );
}

export default TransactionPage;
