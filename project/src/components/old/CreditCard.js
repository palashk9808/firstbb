import React, {useState,useEffect} from "react";
import { Button, ButtonGroup,createTheme,FormGroup, InputLabel,makeStyles,TextField } from "@material-ui/core";
import { ThemeProvider,createGenerateClassName } from "@material-ui/styles";
import { Typography, Container, Paper, Grid } from "@material-ui/core";
import AutoComplete from "./controls/AutoComplete";
import Input from "./controls/Input";
import { BrowserRouter, Link } from "react-router-dom";
import { Checkbox } from "@material-ui/core";
import CheckBox from "./controls/CheckBox";

const useStyles= makeStyles(theme=>({
    root:{
        '& .MuiFormControl-root':{
            width: "80%",
            margin: theme.spacing(1)
        }
    }
}))

const theme = createTheme({
    palette:{
        primary:{
            main:"#2884f0"
        },
        secondary:{
            main: "#40d50a"
        }

    }
})

export default function CreditCard(props){
   
    const [errors, setErrors] = useState({});

    const validate = ()=>{
        let temp={}
        temp.creditName = values.creditName?"":"This field is required"
        temp.creditBank = (/^[a-zA-Z ]*$/).test(values.creditBank)?"":"This field is required"
        temp.creditSwift = values.creditSwift?"":"This field is required"
        temp.creditAC = (/^[0-9]{11}/).test(values.creditAC)?"":"This field is required"
        setErrors({
            ...temp
        })
    }

    const initialValues={
        creditAC:"",
        creditName:"",
        creditSwift:"",
        creditBank:"",
        saveBeneficiary:true

    }
    const [values,setValues]= useState(initialValues);

    


const swifts=[
    {id:"1", title:"IN101010BAN"},
    {id:"2", title:"SG101010SNG"},
    {id:"3", title:"FR101010PAR"},
    {id:"4", title:"US101010NYC"},
    {id:"5", title:"JP101010TKY"}
]
const swiftArr = ["IN101010B","IN101010C","IN101010BAN","IN10101BAN","IN10BAN"]

const banks=[
    {id:"1", title:"SCB Bangalore"},
    {id:"2", title:"SCB Singapore"},
    {id:"3", title:"SCB Paris"},
    {id:"4", title:"SCB New York"},
    {id:"5", title:"SCB Tokyo"}
]


// console.log(values);
const classes= useStyles();

const changeInput=(e)=>{
    const {name,value}=e.target;
    setValues({
        ...values,
        [name]:(value)
    })
    console.log(values)
}
const changeChecked = e =>{
    const val = e.target.checked;
    setValues({
        ...values,
        ["saveBeneficiary"]:val
    })
    console.log(values)
}
const changeSwiftAuto=(e,v)=>{
    console.log("inSwiftAuto", v)

    setValues({
        ...values,
        ["creditSwift"]:(v)?v.title:""
    })
    console.log(values)
}
const changeBankAuto=(e,v)=>{
    console.log("inbankauto", v)
    setValues({
        ...values,
        ["creditBank"]:(v)?v.title:""
    })
    console.log(values)
}

return(
    <div>
        <BrowserRouter>
        <ThemeProvider theme={theme}>
            <form className={classes.root} autoComplete="off">
                <Grid container>
                    <Grid item xs={12} sm={6}>

                        <Input 
                        label="Payee Name"
                         name="creditName" 
                        value={values.creditName} 
                        onChange={changeInput} />

                        <AutoComplete 
                        label="SWIFT ID" 
                        name="creditSwift" 
                        value={values.creditSwift}
                        onChange={changeSwiftAuto}
                        options={swifts} />
                    </Grid>

                    <Grid item xs={12} sm={6}>

                        <Input 
                        label="Account Number" 
                        name="creditAC" 
                        value={values.creditAC} 
                        onChange={changeInput} />

                        <AutoComplete 
                        label="Bank Name" 
                        name="creditBank" 
                        value={values.creditBank} 
                        onChange={changeBankAuto}
                        options={banks} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <CheckBox 
                        onChange={changeChecked}
                        value={values.saveBeneficiary} 
                        label="Save Beneficiary Details"
                        name="saveBeneficiary"/>
                    </Grid>
                </Grid>
            </form>
        </ThemeProvider>
        </BrowserRouter>
    </div>
)
}