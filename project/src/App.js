import './App.css';
import { makeStyles, Paper } from '@material-ui/core';
import {BrowserRouter, Router, Switch, Link} from "react-router-dom"
import TTPage2 from './components/TTPage2';



const useStyles= makeStyles(theme=>({
  pageContent:{
    margin: theme.spacing(5),
    padding: theme.spacing(3)
  }
}))

function App() {
  const classes=useStyles();
  return (
    <div>
        <TTPage2 />
    </div>
  );
}

export default App;
